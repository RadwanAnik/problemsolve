/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teluskotutorial.Array;

import java.util.AbstractList;
import java.util.LinkedList;

/**
 *
 * @author RadwanAnik
 */
public class AbstractListDemo {
    
    
    public static void main(String[] args) {
    
        
        AbstractList<String> list = new LinkedList<String>();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("E");
        
        System.out.println("List" +list);
        
        list.remove(3);
        
        System.out.println("Final AbstractList" +list);
        
        int lastindex = list.indexOf("A");
        
        System.out.println("Last index of A" +lastindex);
        
    }
    
}
