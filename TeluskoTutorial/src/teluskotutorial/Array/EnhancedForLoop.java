/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teluskotutorial.Array;

/**
 *
 * @author RadwanAnik
 */
public class EnhancedForLoop {
    
    public static void main(String[] args) {
         int a[] = new int[5];
         int b[]= {1,2,3,4,5};
        a[0]=1;
        a[1]=3;
        a[2]=4;
        a[3]=8;
        a[4]=6;
        
        
        for(int i : a){
            System.out.print(i);
        }
        
        for(int i : b){
            System.out.print(i+"\n");
        }
    }
}
