/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teluskotutorial.Array;

/**
 *
 * @author RadwanAnik
 */
public class TWO_D_Array {
    
    public static void main(String[] args) {
        
        int x[] ={5,6,7,8};
        int y[] ={6,7,8,4};
        int z[] ={9,8,5,9};
        
        int p[][]={
        
           {5,6,7,8},
           {6,7,8,4},
           {9,8,5,9}    
                
          };
        
        
        for(int i=0;i<3;i++){
            for(int j=0;j<4;j++){
                System.out.print(p[i][j] + " ");
            }
            System.out.println("");
        }
        
        
        
    }
    
}
