/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teluskotutorial;

import java.lang.reflect.Constructor;

/**
 *
 * @author RadwanAnik
 */
public class ReflectionExample {

    private String name;

    public ReflectionExample() {
    }

    public void SetName(String name) {
        this.name = name;

    }

    public static void main(String[] args) {

        try {
            Constructor<ReflectionExample> constructor = ReflectionExample.class.getDeclaredConstructor();
            ReflectionExample r = constructor.newInstance();
            r.SetName("Anik");
            System.out.println(r.name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
